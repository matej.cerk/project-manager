import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'projects', pathMatch: 'full' },
  { path: 'projects', loadChildren: () => import('./projects/feature/projects/projects.module').then(m => m.ProjectsModule) },
  { path: 'projects/:id/edit', loadChildren: () => import('./projects/feature/project-edit/project-edit.module').then(m => m.ProjectEditModule) },
  { path: 'projects/add', loadChildren: () => import('./projects/feature/project-edit/project-edit.module').then(m => m.ProjectEditModule) },
  { path: 'projects/:id/tasks', loadChildren: () => import('./tasks/feature/tasks/tasks.module').then(m => m.TasksModule) },
  { path: 'projects/:id/tasks/:taskId/edit', loadChildren: () => import('./tasks/feature/task-edit/task-edit.module').then(m => m.TaskEditModule) },
  { path: 'projects/:id/tasks/add', loadChildren: () => import('./tasks/feature/task-edit/task-edit.module').then(m => m.TaskEditModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
