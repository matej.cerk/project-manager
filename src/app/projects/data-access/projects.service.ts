import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, of, tap } from 'rxjs';
import {
  getRandomDateWithinYear,
  dateAdd,
} from '../../shared/utils/date-utils';
import dayjs from 'dayjs';

export interface Project {
  id?: number;
  title: string;
  description: string;
  startDate: string;
  endDate: string;
  status: string;
}

export interface SaveProjectDto {
  userId: number;
  id?: number;
  title: string;
  body: string; // we could store json here with all the remaining fields, but since API doesn't work, this is not part of this demo
}

export interface ProjectDto {
  userId: number;
  id: number;
  title: string;
  body: string; // will be used as JSON representing other project properties
}

@Injectable({
  providedIn: 'root',
})
export class ProjectsService {
  private readonly CACHE_DURATION_SECONDS = 60;
  constructor(private http: HttpClient) {}

  private projectsCache: Project[] = [];
  private projectCacheExpiresAt: Date | null = null;
  private setProjectsCache = (projects: Project[]) => {
    this.projectsCache = projects;
    this.projectCacheExpiresAt = dateAdd(new Date(), 'second', this.CACHE_DURATION_SECONDS);
  }

  private get isProjectsCacheStale() {
    if (this.projectsCache.length === 0) return true;
    if (!this.projectCacheExpiresAt) return true;
    if (this.projectCacheExpiresAt < new Date()) return true;
    return false;
  };

  public getProjects(skipCache = false) {
    return this.isProjectsCacheStale && !skipCache ? this.getProjectsFromServer() : of(this.projectsCache);
  }

  public getProjectsFromServer() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts').pipe(
      map((data) => data as ProjectDto[]),
      map((data: ProjectDto[]) => data.map(this.mapToProject)),
      tap(this.setProjectsCache)
    );
  }

  public getProject(id: string, skipCache = false) {
    const skip = this.isProjectsCacheStale && !skipCache;
    if (skip) return this.getProjectFromServer(id);

    const project = this.projectsCache.find((p) => p.id === Number(id));
    return (project) ? of(project) : this.getProjectFromServer(id);
  }

  public getProjectFromServer(id: string) {
    return this.http.get(`https://jsonplaceholder.typicode.com/posts/${id}`).pipe(
      map((data) => data as ProjectDto),
      map(this.mapToProject)
    );
  }

  public saveProject(project: Project) {
    const saveProjectDto: SaveProjectDto = this.mapToSaveProjectDto(project);
    const shouldInsert = saveProjectDto.id === undefined || saveProjectDto.id === null;
    return shouldInsert ? this.addProject(saveProjectDto) : this.updateProject(saveProjectDto);
  }

  private addProject(saveProjectDto: SaveProjectDto) {
    return this.http.post(
      'https://jsonplaceholder.typicode.com/posts',
      saveProjectDto
    );
  }

  private updateProject(saveProjectDto: SaveProjectDto) {
    return this.http.put(
      `https://jsonplaceholder.typicode.com/posts/${saveProjectDto.id}`,
      saveProjectDto
    );
  }

  public deleteProject(id: number) {
    return this.http.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
  }

  private mapToSaveProjectDto(project: Project): SaveProjectDto {
    return {
      userId: 1, // we don't have user management in this demo
      id: project.id,
      title: project.title,
      body: project.description,
    };
  }

  private mapToProject(post: ProjectDto): Project {
    // mocked
    const startDate = getRandomDateWithinYear();
    const endDate = dateAdd(startDate, 'month', 1);
    const status = Math.random() > 0.5 ? 'In Progress' : 'Completed';
    // end mocked

    return {
      id: post.id,
      title: post.title,
      description: post.body,
      startDate: dayjs(startDate).format("MM-DD-YYYY"),
      endDate: dayjs(endDate).format("MM-DD-YYYY"),
      status: status,
    } as Project;
  }
}
