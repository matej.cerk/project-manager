import { Component, inject } from '@angular/core';
import {
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn,
} from '@angular/forms';
import { Project, ProjectsService } from '../../data-access/projects.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../../shared/ui/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-project-edit',
  templateUrl: './project-edit.component.html',
  styleUrl: './project-edit.component.scss',
})
export class ProjectEditComponent {
  public isSaving = false;
  public isLoading = false;
  private projectsService = inject(ProjectsService);
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private dialog = inject(MatDialog);

  public statusOptions = ['In Progress', 'Completed'].map((status) => ({
    key: status,
    value: status,
  }));

  public get breadcrumbs() {
    return [
      { label: 'projects', routerLink: '/projects' },
      {
        label: this.isEditMode ? 'Edit Project' : 'New Project',
        routerLink: '',
      },
    ];
  }

  public get isDisabled() {
    return this.isSaving || this.isLoading;
  }

  public get isSaveDisabled() {
    return !this.isValid || this.isDisabled;
  }

  public get isEditMode() {
    return this.id !== undefined && this.id !== null;
  }

  private get id() {
    return this.activatedRoute.snapshot.params['id'] ?? undefined;
  }

  public projectForm = new FormGroup(
    {
      title: new FormControl('', Validators.required),
      description: new FormControl(''),
      startDate: new FormControl(undefined as Date | undefined, [
        Validators.required,
      ]),
      endDate: new FormControl(undefined as Date | undefined, [
        Validators.required,
      ]),
      status: new FormControl('', Validators.required),
    },
    { validators: this.dateRangeValidator as ValidatorFn }
  );

  dateRangeValidator(formGroup: FormGroup) {
    const startDateControl = formGroup.get('startDate');
    const endDateControl = formGroup.get('endDate');
    const startDate = startDateControl?.value;
    const endDate = endDateControl?.value;

    const dateRangeInvalid =
      startDate != undefined && endDate != undefined && startDate > endDate;
    if (dateRangeInvalid) {
      endDateControl?.setErrors({ dateRangeInvalid: dateRangeInvalid });
    } else if (endDateControl?.valid) {
      endDateControl?.setErrors(null);
    } else {
      endDateControl?.setErrors(
        Object.assign({}, endDateControl?.errors, {
          dateRangeInvalid: dateRangeInvalid,
        })
      );
    }
  }

  constructor() {
    this.activatedRoute.params.subscribe((params) => {
      const id = params['id'];
      if (id) {
        this.loadProject(id);
      }
    });
  }

  private loadProject(id: string) {
    this.isLoading = true;
    this.projectsService.getProject(id).subscribe({
      next: (project) => {
        const value = this.toFormValue(project);
        this.projectForm.patchValue(value);
        this.isLoading = false;
      },
      error: (error) => {
        console.error('Error loading project', error);
        this.isLoading = false;
      },
    });
  }

  public async viewTasks(event: Event) {
    event.preventDefault();
    this.router.navigate(['projects', this.id, 'tasks']);
  }

  private get isValid() {
    return this.projectForm.valid && this.projectForm.dirty;
  }

  public async saveProject() {
    if (!this.isValid) {
      return;
    }
    this.isSaving = true;
    const project = this.toProject(this.projectForm.value);
    this.projectsService.saveProject(project).subscribe({
      complete: () => this.saveSuccess(),
      error: this.saveError,
    });
  }

  public async cancelSave(event: Event) {
    event.preventDefault();
    this.router.navigate(['projects']);
  }

  public async deleteProject(event: Event) {
    event.preventDefault();
    const confirmed = await this.confirmDelete();
    if (!confirmed) {
      return;
    }
    this.isSaving = true;
    const project = this.toProject(this.projectForm.value);
    this.projectsService.deleteProject(project.id).subscribe({
      complete: () => this.saveSuccess(),
      error: this.saveError,
    });
  }

  private async confirmDelete(): Promise<boolean> {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Are you sure you want to delete this project?',
        message: 'Please write DELETE to confirm.',
        compareTo: 'DELETE',
      },
    });
    return dialogRef.afterClosed().toPromise();
  }

  private saveSuccess() {
    this.isSaving = false;
    this.router.navigate(['projects']);
  }

  private saveError(error: any) {
    this.isSaving = false;
    console.error('Error saving project', error);
  }

  private toProject(formValue: any) {
    return {
      id: this.id,
      title: formValue.title,
      description: formValue.description,
      startDate: formValue.startDate,
      endDate: formValue.endDate,
      status: formValue.status,
    };
  }

  private toFormValue(project: Project) {
    return {
      title: project.title,
      description: project.description,
      startDate: new Date(project.startDate),
      endDate: new Date(project.endDate),
      status: project.status,
    };
  }
}
