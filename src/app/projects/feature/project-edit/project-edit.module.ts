import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProjectEditRoutingModule } from './project-edit-routing.module';
import { ProjectEditComponent } from './project-edit.component';

import { ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { provideNativeDateAdapter } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { FormInputComponent } from '../../../shared/ui/forms/form-input/form-input.component';
import { FormTextareaComponent } from '../../../shared/ui/forms/form-textarea/form-textarea.component';
import { FormDatepickerComponent } from '../../../shared/ui/forms/form-datepicker/form-datepicker.component';
import { FormSelectComponent } from '../../../shared/ui/forms/form-select/form-select.component';
import { BreadcrumbComponent } from '../../../shared/ui/breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [ProjectEditComponent],
  providers: [provideNativeDateAdapter()],
  imports: [
    CommonModule,
    ProjectEditRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    FormInputComponent,
    FormTextareaComponent,
    FormDatepickerComponent,
    FormSelectComponent,
    BreadcrumbComponent
  ],
})
export class ProjectEditModule {}
