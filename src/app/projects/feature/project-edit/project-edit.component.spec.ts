import { provideHttpClient, withFetch } from '@angular/common/http';
import { ProjectEditComponent } from './project-edit.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatOptionModule, provideNativeDateAdapter } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationDialogComponent } from '../../../shared/ui/confirmation-dialog/confirmation-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { FormInputComponent } from '../../../shared/ui/forms/form-input/form-input.component';
import { FormDatepickerComponent } from '../../../shared/ui/forms/form-datepicker/form-datepicker.component';
import { FormTextareaComponent } from '../../../shared/ui/forms/form-textarea/form-textarea.component';
import { FormSelectComponent } from '../../../shared/ui/forms/form-select/form-select.component';
import { BreadcrumbComponent } from '../../../shared/ui/breadcrumb/breadcrumb.component';

describe('ProjectEditComponent', () => {
  let component: ProjectEditComponent;
  let fixture: ComponentFixture<ProjectEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ProjectEditComponent],
      providers: [provideHttpClient(withFetch()), provideNativeDateAdapter()],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        ReactiveFormsModule,
        ConfirmationDialogComponent,
        MatIconModule,
        FormInputComponent,
        FormDatepickerComponent,
        FormTextareaComponent,
        FormSelectComponent,
        BreadcrumbComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ProjectEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test title (empty)', () => {
    const input = component.projectForm.controls.title;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test start date (empty)', () => {
    const input = component.projectForm.controls.startDate;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test end date (empty)', () => {
    const input = component.projectForm.controls.endDate;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test status (empty)', () => {
    const input = component.projectForm.controls.status;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });


  it('test title (entered)', () => {
    const input = component.projectForm.controls.title;
    input.setValue('Test Title');
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test start date (entered)', () => {
    const input = component.projectForm.controls.startDate;
    input.setValue(new Date());
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test end date (entered)', () => {
    const input = component.projectForm.controls.endDate;
    let date = new Date();
    date.setFullYear(date.getFullYear() + 1);
    input.setValue(date);
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test status (entered)', () => {
    const input = component.projectForm.controls.status;
    input.setValue('In Progress');
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test date range invalid', () => {
    const form = component.projectForm;
    let date1 = new Date();
    let date2 = new Date();
    date2.setFullYear(date2.getFullYear() - 1);
    form.controls.startDate.setValue(date1);
    form.controls.endDate.setValue(date2);
    form.updateValueAndValidity();
    expect(form.valid).toBeFalsy();
    expect(form.controls.endDate.hasError('dateRangeInvalid')).toBeTruthy();
  });

  it('test form output', () => {
    const form = component.projectForm;
    form.controls.title.setValue('Test Title');
    form.controls.description.setValue('Test Description');
    let date1 = new Date();
    form.controls.startDate.setValue(date1);
    let date2 = new Date();
    date2.setFullYear(date2.getFullYear() + 1);
    form.controls.endDate.setValue(date2);
    form.controls.status.setValue('In Progress');

    expect(form.valid).toBeTruthy();
    expect(form.hasError('required')).toBeFalsy();
    expect(form.value).toEqual({
      title: 'Test Title',
      description: 'Test Description',
      startDate: date1,
      endDate: date2,
      status: 'In Progress',
    });
  });

});
