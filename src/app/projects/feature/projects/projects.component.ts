import { Component, inject } from '@angular/core';
import { Project, ProjectsService } from '../../data-access/projects.service';
import { map } from 'rxjs';
import { GridData } from '../../../shared/ui/grid/grid.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrl: './projects.component.scss',
})
export class ProjectsComponent {
  private projectsService = inject(ProjectsService);
  private router = inject(Router);

  public projects$ = this.projectsService.getProjects();

  public projectsGridData$ = this.projects$.pipe(
    map((projects) => this.toGridData(projects))
  );

  public editProject(project: Project) {
    this.router.navigate(['projects', project.id, 'edit']);
  }

  private projectGridHeaders: any = {
    title: 'Title',
    description: 'Description',
    startDate: 'Start Date',
    endDate: 'End Date',
    status: 'Status',
  };

  private toGridData(projects: Project[]): GridData {
    return {
      columns: projects.length
        ? Object.keys(projects[0])
            .filter((key) => key != 'id')
            .map((key) => ({ key, label: this.projectGridHeaders[key] }))
        : [],
      rows: projects as any[],
    } as GridData;
  }
}
