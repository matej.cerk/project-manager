import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';

export interface Task {
  id?: number;
  title: string;
  description: string;
  estimatedTimeHours: number;
  assignee: string;
  status: string;
}

export interface SaveTaskDto {
  userId: number;
  id?: number;
  title: string;
  completed: boolean;
}

export interface TaskDto {
  userId: number;
  id: number;
  title: string;
  completed: boolean;
}

@Injectable({
  providedIn: 'root',
})
export class TasksService {
  constructor(private http: HttpClient) {}

  public getTasks() {
    return this.http.get('https://jsonplaceholder.typicode.com/todos').pipe(
      map((data) => data as TaskDto[]),
      map((data: TaskDto[]) => data.map(this.mapToTask))
    );
  }

  public getTask(id: string) {
    return this.http.get(`https://jsonplaceholder.typicode.com/todos/${id}`).pipe(
      map((data) => data as TaskDto),
      map(this.mapToTask)
    );
  }

  public saveTask(Task: Task) {
    const saveTaskDto: SaveTaskDto = this.mapToSaveTaskDto(Task);
    const shouldInsert = saveTaskDto.id === undefined || saveTaskDto.id === null;
    return shouldInsert ? this.addTask(saveTaskDto) : this.updateTask(saveTaskDto);
  }

  private addTask(saveTaskDto: SaveTaskDto) {
    return this.http.post(
      'https://jsonplaceholder.typicode.com/todos',
      saveTaskDto
    );
  }

  private updateTask(saveTaskDto: SaveTaskDto) {
    return this.http.put(
      `https://jsonplaceholder.typicode.com/todos/${saveTaskDto.id}`,
      saveTaskDto
    );
  }

  public deleteTask(id: number) {
    return this.http.delete(`https://jsonplaceholder.typicode.com/todos/${id}`);
  }

  private mapToSaveTaskDto(task: Task): SaveTaskDto {
    return {
      userId: 1, // we don't have user management in this demo
      id: task.id,
      title: task.title,
      completed: task.status === 'Completed',
    };
  }

  private mapToTask(task: TaskDto): Task {
    // mocked
    const description = task.title + task.title;
    const estimatedTimeHours = Math.floor(Math.random() * 100);
    const assignee = Math.random() > 0.5 ? "John Doe" : "Susan Smith";
    const status = Math.random() > 0.75 ? 'Todo' : Math.random() > 0.5 ? 'In Progress' : 'Completed';
    // end mocked
    
    return {
      id: task.id,
      title: task.title,
      description: description,
      estimatedTimeHours: estimatedTimeHours,
      assignee: assignee,
      status: status,
    } as Task;
  }
}
