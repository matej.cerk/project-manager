import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksComponent } from './tasks.component';
import { GridComponent } from '../../../shared/ui/grid/grid.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { BreadcrumbComponent } from '../../../shared/ui/breadcrumb/breadcrumb.component';


@NgModule({
  declarations: [
    TasksComponent
  ],
  imports: [
    CommonModule,
    TasksRoutingModule,
    GridComponent,
    MatIconModule,
    MatButtonModule,
    BreadcrumbComponent
  ]
})
export class TasksModule { }
