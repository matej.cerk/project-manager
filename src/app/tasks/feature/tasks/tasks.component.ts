import { Component, inject } from '@angular/core';
import { Task, TasksService } from '../../data-access/tasks.service';
import { map, take } from 'rxjs';
import { GridData } from '../../../shared/ui/grid/grid.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Project, ProjectsService } from '../../../projects/data-access/projects.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrl: './tasks.component.scss',
})
export class TasksComponent {
  private projectsService = inject(ProjectsService);
  private tasksService = inject(TasksService);
  private router = inject(Router);

  private activatedRoute = inject(ActivatedRoute);

  private get projectId() {
    return this.activatedRoute.snapshot.params['id'] ?? undefined;
  }

  private project: Project | null = null;

  public get breadcrumbs() {
    if (this.project === undefined || this.project === null) {
      return null;
    }

    return [
      { label: 'projects', routerLink: '/projects' },
      {
        label: this.project.title,
        routerLink: `/projects/${this.projectId}/edit`,
      },
      { label: 'tasks', routerLink: '' },
    ];
  }

  constructor() {
    this.activatedRoute.params.subscribe((params) => {
      const projectId = params['id'];
      if (projectId) {
        this.projectsService
          .getProject(projectId)
          .pipe(take(1))
          .subscribe((result) => (this.project = result));
      }
    });
  }

  public tasks$ = this.tasksService.getTasks();

  public tasksGridData$ = this.tasks$.pipe(
    map((tasks) => this.toGridData(tasks))
  );

  public editTask(task: Task) {
    console.log(['projects', this.projectId, 'tasks', task.id, 'edit']);
    this.router.navigate([
      'projects',
      this.projectId,
      'tasks',
      task.id,
      'edit',
    ]);
  }

  private projectGridHeaders: any = {
    title: 'Title',
    description: 'Description',
    estimatedTimeHours: 'Estimated time (hours)',
    assignee: 'Assignee',
    status: 'Status',
  };

  private toGridData(tasks: Task[]): GridData {
    return {
      columns: tasks.length
        ? Object.keys(tasks[0])
            .filter((key) => key != 'id')
            .map((key) => ({ key, label: this.projectGridHeaders[key] }))
        : [],
      rows: tasks as any[],
    } as GridData;
  }
}
