import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaskEditRoutingModule } from './task-edit-routing.module';
import { TaskEditComponent } from './task-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { provideNativeDateAdapter, MatOptionModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { FormDatepickerComponent } from '../../../shared/ui/forms/form-datepicker/form-datepicker.component';
import { FormInputComponent } from '../../../shared/ui/forms/form-input/form-input.component';
import { FormSelectComponent } from '../../../shared/ui/forms/form-select/form-select.component';
import { FormTextareaComponent } from '../../../shared/ui/forms/form-textarea/form-textarea.component';
import { BreadcrumbComponent } from '../../../shared/ui/breadcrumb/breadcrumb.component';


@NgModule({
  declarations: [
    TaskEditComponent
  ],
  providers: [provideNativeDateAdapter()],
  imports: [
    CommonModule,
    TaskEditRoutingModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatSelectModule,
    MatOptionModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatIconModule,
    FormInputComponent,
    FormTextareaComponent,
    FormDatepickerComponent,
    FormSelectComponent,
    BreadcrumbComponent
  ],
})
export class TaskEditModule { }
