import { Component, inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Task, TasksService } from '../../data-access/tasks.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfirmationDialogComponent } from '../../../shared/ui/confirmation-dialog/confirmation-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { Project, ProjectsService } from '../../../projects/data-access/projects.service';
import { map, of, take } from 'rxjs';

@Component({
  selector: 'app-task-edit',
  templateUrl: './task-edit.component.html',
  styleUrl: './task-edit.component.scss',
})
export class TaskEditComponent {
  public isSaving = false;
  public isLoading = false;
  private projectsService = inject(ProjectsService);
  private tasksService = inject(TasksService);
  private router = inject(Router);
  private activatedRoute = inject(ActivatedRoute);
  private dialog = inject(MatDialog);

  public statusOptions = ['Todo', 'In Progress', 'Completed'].map((status) => ({
    key: status,
    value: status,
  }));

  private project: Project | null = null;

  public get breadcrumbs() {
    if (this.project === undefined || this.project === null) {
      return null;
    }

    return [
      { label: 'Projects', routerLink: '/projects' },
      {
        label: this.project.title,
        routerLink: `/projects/${this.projectId}/edit`,
      },
      { label: 'Tasks', routerLink: `/projects/${this.projectId}/tasks` },
      { label: this.isEditMode ? 'Edit Task' : 'New Task', routerLink: '' },
    ];
  }

  public get isDisabled() {
    return this.isSaving || this.isLoading;
  }

  public get isSaveDisabled() {
    return !this.isValid || this.isDisabled;
  }

  public get isEditMode() {
    return this.taskId !== undefined && this.taskId !== null;
  }

  public get projectId() {
    return this.activatedRoute.snapshot.params['id'] ?? undefined;
  }

  private get taskId() {
    return this.activatedRoute.snapshot.params['taskId'] ?? undefined;
  }

  public taskForm = new FormGroup({
    title: new FormControl('', Validators.required),
    description: new FormControl(''),
    estimatedTimeHours: new FormControl(undefined as number | undefined, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
    ]),
    assignee: new FormControl('', [Validators.required]),
    status: new FormControl('', Validators.required),
  });

  constructor() {
    this.activatedRoute.params.subscribe((params) => {
      const taskId = params['taskId'];
      if (taskId) {
        this.loadTask(taskId);
      }

      const projectId = params['id'];
      if (projectId) {
        this.projectsService
          .getProject(projectId)
          .pipe(take(1))
          .subscribe((result) => (this.project = result));
      }
    });
  }

  private loadTask(id: string) {
    this.isLoading = true;
    this.tasksService.getTask(id).subscribe({
      next: (task) => {
        const value = this.toFormValue(task);
        this.taskForm.patchValue(value);
        this.isLoading = false;
      },
      error: (error) => {
        console.error('Error loading task', error);
        this.isLoading = false;
      },
    });
  }

  private get isValid() {
    return this.taskForm.valid && this.taskForm.dirty;
  }

  public async saveTask() {
    this.isSaving = true;
    const task = this.toTask(this.taskForm.value);
    this.tasksService.saveTask(task).subscribe({
      complete: () => this.saveSuccess(),
      error: this.saveError,
    });
  }

  public async cancelSave(event: Event) {
    event.preventDefault();
    this.router.navigate(['projects', this.projectId, 'tasks']);
  }

  public async deleteTask(event: Event) {
    event.preventDefault();
    const confirmed = await this.confirmDelete();
    if (!confirmed) {
      return;
    }
    this.isSaving = true;
    const task = this.toTask(this.taskForm.value);
    this.tasksService.deleteTask(task.id).subscribe({
      complete: () => this.saveSuccess(),
      error: this.saveError,
    });
  }

  private async confirmDelete(): Promise<boolean> {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      data: {
        title: 'Are you sure you want to delete this task?',
        message: 'Please write DELETE to confirm.',
        compareTo: 'DELETE',
      },
    });
    return dialogRef.afterClosed().toPromise();
  }

  private saveSuccess() {
    this.isSaving = false;
    console.log('Task saved', this.projectId);
    this.router.navigate(['projects', this.projectId, 'tasks']);
  }

  private saveError(error: any) {
    this.isSaving = false;
    console.error('Error saving task', error);
  }

  private toTask(formValue: any) {
    return {
      id: this.taskId,
      title: formValue.title,
      description: formValue.description,
      estimatedTimeHours: formValue.estimatedTimeHours,
      assignee: formValue.assignee,
      status: formValue.status,
    };
  }

  private toFormValue(task: Task) {
    return {
      title: task.title,
      description: task.description,
      estimatedTimeHours: task.estimatedTimeHours,
      assignee: task.assignee,
      status: task.status,
    };
  }
}
