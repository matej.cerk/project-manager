import { provideHttpClient, withFetch } from '@angular/common/http';
import { TaskEditComponent } from './task-edit.component';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatOptionModule, provideNativeDateAdapter } from '@angular/material/core';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationDialogComponent } from '../../../shared/ui/confirmation-dialog/confirmation-dialog.component';
import { MatIconModule } from '@angular/material/icon';
import { FormDatepickerComponent } from '../../../shared/ui/forms/form-datepicker/form-datepicker.component';
import { FormInputComponent } from '../../../shared/ui/forms/form-input/form-input.component';
import { FormSelectComponent } from '../../../shared/ui/forms/form-select/form-select.component';
import { FormTextareaComponent } from '../../../shared/ui/forms/form-textarea/form-textarea.component';
import { BreadcrumbComponent } from '../../../shared/ui/breadcrumb/breadcrumb.component';

describe('TaskEditComponent', () => {
  let component: TaskEditComponent;
  let fixture: ComponentFixture<TaskEditComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [TaskEditComponent],
      providers: [provideHttpClient(withFetch()), provideNativeDateAdapter()],
      imports: [
        RouterTestingModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
        MatDatepickerModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        ReactiveFormsModule,
        ConfirmationDialogComponent,
        MatIconModule,
        FormInputComponent,
        FormTextareaComponent,
        FormDatepickerComponent,
        FormSelectComponent,
        BreadcrumbComponent
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(TaskEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('test title (empty)', () => {
    const input = component.taskForm.controls.title;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test est. hours (empty)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test est. hours (not a number)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    input.setValue('abc' as any);
    expect(input.valid).toBeFalsy();
  });

  it('test est. hours (12.2)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    input.setValue('12.2' as any);
    expect(input.valid).toBeFalsy();
  });

  it('test est. hours (12,2)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    input.setValue('12,2' as any);
    expect(input.valid).toBeFalsy();
  });

  it('test est. hours (number entered as string)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    input.setValue('12' as any);
    expect(input.valid).toBeTruthy();
  });

  it('test assignee (empty)', () => {
    const input = component.taskForm.controls.assignee;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });

  it('test status (empty)', () => {
    const input = component.taskForm.controls.status;
    expect(input.valid).toBeFalsy();
    expect(input.hasError('required')).toBeTruthy();
  });


  it('test title (entered)', () => {
    const input = component.taskForm.controls.title;
    input.setValue('Test Title');
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test est. hours (entered)', () => {
    const input = component.taskForm.controls.estimatedTimeHours;
    input.setValue(123);
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test assignee (entered)', () => {
    const input = component.taskForm.controls.assignee;
    input.setValue("John Doe");
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });

  it('test status (entered)', () => {
    const input = component.taskForm.controls.status;
    input.setValue('In Progress');
    expect(input.valid).toBeTruthy();
    expect(input.hasError('required')).toBeFalsy();
  });


  it('test form output', () => {
    const form = component.taskForm;
    form.controls.title.setValue('Test Title');
    form.controls.description.setValue('Test Description');
    form.controls.estimatedTimeHours.setValue(123);
    form.controls.assignee.setValue("John Doe");
    form.controls.status.setValue('In Progress');

    expect(form.valid).toBeTruthy();
    expect(form.hasError('required')).toBeFalsy();
    expect(form.value).toEqual({
      title: 'Test Title',
      description: 'Test Description',
      estimatedTimeHours: 123,
      assignee: "John Doe",
      status: 'In Progress',
    });
  });
});
