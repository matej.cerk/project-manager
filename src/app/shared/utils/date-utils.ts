export function getRandomDateWithinYear(): Date {
    const currentYear = new Date().getFullYear();
    const startOfYear = new Date(currentYear, 0, 1).getTime();
    const endOfYear = new Date(currentYear, 11, 31).getTime();
    const randomTime = startOfYear + Math.random() * (endOfYear - startOfYear);
    return new Date(randomTime);
}

export function dateAdd(date: Date, interval: string, amount: number): Date {
    const newDate = new Date(date);
    switch (interval.toLowerCase()) {
        case 'year':
            newDate.setFullYear(newDate.getFullYear() + amount);
            break;
        case 'month':
            newDate.setMonth(newDate.getMonth() + amount);
            break;
        case 'day':
            newDate.setDate(newDate.getDate() + amount);
            break;
        case 'hour':
            newDate.setHours(newDate.getHours() + amount);
            break;
        case 'minute':
            newDate.setMinutes(newDate.getMinutes() + amount);
            break;
        case 'second':
            newDate.setSeconds(newDate.getSeconds() + amount);
            break;
        case 'millisecond':
            newDate.setMilliseconds(newDate.getMilliseconds() + amount);
            break;
        default:
            throw new Error('Invalid interval');
    }
    return newDate;
}