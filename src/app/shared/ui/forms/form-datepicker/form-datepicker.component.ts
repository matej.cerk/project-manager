import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';

@Component({
  selector: 'app-form-datepicker',
  templateUrl: './form-datepicker.component.html',
  styleUrl: './form-datepicker.component.scss',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatIconModule,
    MatInputModule,
    ReactiveFormsModule
  ],
})
export class FormDatepickerComponent {
  @Input() label: string = '';
  @Input() fieldName: string = '';
  @Input() dataTest: string= '';
  @Input() control!: FormControl;
  @Input() errorMessage: string = '';
  @Input() cssClass: string= '';
}
