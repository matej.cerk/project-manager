import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatSelectModule } from '@angular/material/select';

interface DropdownOption {
  key: string;
  value: string;
}

@Component({
  selector: 'app-form-select',
  templateUrl: './form-select.component.html',
  styleUrl: './form-select.component.scss',
  standalone: true,
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatIconModule,
    ReactiveFormsModule
  ],
})
export class FormSelectComponent {
  @Input() label: string = '';
  @Input() fieldName: string = '';
  @Input() dataTest: string= '';
  @Input() control!: FormControl;
  @Input() errorMessage: string = '';
  @Input() cssClass: string= '';
  @Input() dropdownOptions: DropdownOption[] = [];
}
