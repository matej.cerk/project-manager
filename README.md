# ProjectManager

This is a demo app that displays projects and tasks. Allows CRUD operations on both projects and tasks.

- it uses folder and architectural structure inspired by NX Angular
- smart components under features folders, and dumb components under ui folders
- fetches data from mock API and caches it for 60 seconds within the app
- end to end testing main CRUD features by cypress
- unit tests for components and form validations
- lazy loading of modules (each route) and standalone components for shared components
- angular material v3 and bootstrap. the latter could be easily removed to reduce bundle size
- custom material theme, which is settable in m3-theme.scss
- ssr not tested within the demo
- mock API responses are re-mapped to the view models where non-existing properties are populated randomly

You can check out the live demo of the app [here](https://6668aa7f79dfcf006d673696--aesthetic-centaur-df6d03.netlify.app/).

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build --ssr=false` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests (with gui).
Run `npm run cypress:run` to execute headless.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build --ssr=false` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` or `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests (with gui).
Run `npm run cypress:run` to execute headless.
