export class ProjectsPage {
    navigateTo() {
        return cy.visit('/projects');
    }
  
    clickListItem(index: number) {
      cy.wait(100); // wait for list to load
      return cy.get('[data-test="projects-list"] .list-item').eq(index).click();
    }

    getListHeader() {
      return cy.get('[data-test="projects-list"] .list-header');
    }

    getListItems() {
      return cy.get('[data-test="projects-list"] .list-item');
    }
  }