export class ProjectEditPage {
  navigateToEdit(id: number) {
    return cy.visit(`/projects/${id}/edit`);
  }

  navigateToAdd() {
    return cy.visit(`/projects/add`);
  }

  fillForm() {
    cy.get('[data-test="project-title"]').type('Project 1');
    cy.get('[data-test="project-description"]').type('Project description 1');
    cy.get('[data-test="project-startDate"]').clear();
    cy.get('[data-test="project-startDate"]').type('6/12/2024');
    cy.get('[data-test="project-endDate"]').clear();
    cy.get('[data-test="project-endDate"]').type('12/25/2024');
    // click select
    cy.get('[data-test="project-status"]').click()
    cy.get('mat-option').contains('In Progress').click();
    return true;
  }

  clickSave() {
    return cy.get('[data-test="project-save"]').click();
  }

  clickDelete() {
    cy.wait(100); // wait for button to be displayed
    return cy.get('[data-test="project-delete"]').click();
  }

  confirmDelete(confirm: boolean = true) {
    cy.wait(100); // wait for animation
    if (confirm) {
      cy.get('[data-test="confirmation-prompt"]').type('D');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-prompt"]').type('L');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-prompt"]').type('T');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-yes"]').click();
    } else {
      cy.get('[data-test="confirmation-no"]').click();
    }
    return true;
  }
}
