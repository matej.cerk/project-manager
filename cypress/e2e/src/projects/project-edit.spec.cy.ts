import { ProjectEditPage } from './project-edit.po';
import { ProjectsPage } from './projects.po';

describe('Project edit', () => {
  let page: ProjectsPage;
  let pageEdit: ProjectEditPage;

  beforeEach(() => {
    page = new ProjectsPage();
    pageEdit = new ProjectEditPage();
  });

  it('Edits one project', () => {
    pageEdit.navigateToEdit(1);
    pageEdit.fillForm();
    cy.intercept('PUT', 'https://jsonplaceholder.typicode.com/posts/*', {
      statusCode: 201,
    }).as('editProject');
    pageEdit.clickSave();
    cy.wait('@editProject').its('response.statusCode').should('eq', 201);
  });

  it('Adds one project', () => {
    pageEdit.navigateToAdd();
    pageEdit.fillForm();
    cy.intercept('POST', 'https://jsonplaceholder.typicode.com/posts', {
      statusCode: 201,
    }).as('editProject');
    pageEdit.clickSave();
    cy.wait('@editProject').its('response.statusCode').should('eq', 201);
  });

  it('Deletes one project', () => {
    page.navigateTo();
    page.clickListItem(0);
    pageEdit.clickDelete();
    cy.intercept('DELETE', 'https://jsonplaceholder.typicode.com/posts/*', {
      statusCode: 200,
    }).as('deleteProject');
    pageEdit.confirmDelete();
    cy.wait('@deleteProject').its('response.statusCode').should('eq', 200);
  });
  
  it('Tries to delete one project but cancels', () => {
    page.navigateTo();
    page.clickListItem(0);
    pageEdit.clickDelete();
    cy.intercept('DELETE', 'https://jsonplaceholder.typicode.com/posts/*', {
      statusCode: 200,
    }).as('deleteProject');
    pageEdit.confirmDelete(false);
    // cy.wait('@deleteProject').should('not.have.property', 'request');
  });
});
