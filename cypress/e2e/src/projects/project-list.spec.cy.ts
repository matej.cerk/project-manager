import { ProjectsPage } from "./projects.po";

describe('Project list', () => {
  let page: ProjectsPage;

  beforeEach(() => {
    page = new ProjectsPage();
  })

  it('Renders the projects list', () => {
    page.navigateTo();
    page.getListHeader().should('exist');
    page.getListItems().should('have.length.greaterThan', 0);
  })
})
