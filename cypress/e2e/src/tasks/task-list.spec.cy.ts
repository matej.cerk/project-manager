import { TasksPage } from "./tasks.po";

describe('Tasks list', () => {
  let page: TasksPage;

  beforeEach(() => {
    page = new TasksPage();
  })

  it('Renders the tasks list', () => {
    page.navigateTo();
    page.getListHeader().should('exist');
    page.getListItems().should('have.length.greaterThan', 0);
  })
})
