export class TasksPage {
    navigateTo() {
        return cy.visit('/projects/1/tasks');
    }
  
    clickListItem(index: number) {
      cy.wait(100); // wait for list to be displayed
      return cy.get('[data-test="tasks-list"] .list-item').eq(index).click();
    }

    getListHeader() {
      return cy.get('[data-test="tasks-list"] .list-header');
    }

    getListItems() {
      return cy.get('[data-test="tasks-list"] .list-item');
    }
  }