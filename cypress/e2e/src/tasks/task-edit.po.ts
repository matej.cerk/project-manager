export class TaskEditPage {
  navigateToEdit(projectId: number, id: number) {
    return cy.visit(`/projects/${projectId}/tasks/${id}/edit`);
  }

  navigateToAdd(projectId: number) {
    return cy.visit(`/projects/${projectId}/tasks/add`);
  }

  fillForm() {
    cy.get('[data-test="task-title"]').type('Task 1');
    cy.get('[data-test="task-description"]').type('Task description 1');
    cy.get('[data-test="task-estimatedTimeHours"]').type('12');
    cy.get('[data-test="task-assignee"]').type('Joshua George');
    // click select
    cy.get('[data-test="task-status"]').click()
    cy.get('mat-option').contains('In Progress').click();
    return true;
  }

  clickSave() {
    return cy.get('[data-test="task-save"]').click();
  }

  clickDelete() {
    cy.wait(100); // wait for button to be displayed
    return cy.get('[data-test="task-delete"]').click();
  }

  confirmDelete(confirm: boolean = true) {
    cy.wait(100); // wait for animation
    if (confirm) {
      cy.get('[data-test="confirmation-prompt"]').type('D');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-prompt"]').type('L');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-prompt"]').type('T');
      cy.get('[data-test="confirmation-prompt"]').type('E');
      cy.get('[data-test="confirmation-yes"]').click();
    } else {
      cy.get('[data-test="confirmation-no"]').click();
    }
    return true;
  }
}
