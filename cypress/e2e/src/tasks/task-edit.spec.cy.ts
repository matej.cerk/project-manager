import { TaskEditPage } from './task-edit.po';
import { TasksPage } from './tasks.po';

describe('Task edit', () => {
  let page: TasksPage;
  let pageEdit: TaskEditPage;

  beforeEach(() => {
    page = new TasksPage();
    pageEdit = new TaskEditPage();
  });

  it('Edits one task', () => {
    pageEdit.navigateToEdit(1, 1);
    pageEdit.fillForm();
    cy.intercept('PUT', 'https://jsonplaceholder.typicode.com/todos/*', {
      statusCode: 201,
    }).as('editTask');
    pageEdit.clickSave();
    cy.wait('@editTask').its('response.statusCode').should('eq', 201);
  });

  it('Adds one task', () => {
    pageEdit.navigateToAdd(1);
    pageEdit.fillForm();
    cy.intercept('POST', 'https://jsonplaceholder.typicode.com/todos', {
      statusCode: 201,
    }).as('editTask');
    pageEdit.clickSave();
    cy.wait('@editTask').its('response.statusCode').should('eq', 201);
  });

  it('Deletes one task', () => {
    page.navigateTo();
    page.clickListItem(0);
    pageEdit.clickDelete();
    cy.intercept('DELETE', 'https://jsonplaceholder.typicode.com/todos/*', {
      statusCode: 200,
    }).as('deleteTask');
    pageEdit.confirmDelete();
    cy.wait('@deleteTask').its('response.statusCode').should('eq', 200);
  });
  
  it('Tries to delete one task but cancels', () => {
    page.navigateTo();
    page.clickListItem(0);
    pageEdit.clickDelete();
    cy.intercept('DELETE', 'https://jsonplaceholder.typicode.com/todos/*', {
      statusCode: 200,
    }).as('deleteTask');
    pageEdit.confirmDelete(false);
    // cy.wait('@deleteTask').should('not.have.property', 'request');
  });
});
