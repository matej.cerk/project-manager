export class AppPage {
    navigateTo() {
        return cy.visit('/');
    }
  
    getHeaderText(text: string) {
      return cy.contains(text);
    }
  }