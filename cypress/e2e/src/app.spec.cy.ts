import { AppPage } from "./app.po";

describe('App Tests', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  })

  it('Visits the initial project page', () => {
    page.navigateTo();
    page.getHeaderText('Project Manager');
  })
})
